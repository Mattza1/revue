import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Module from "../views/Module.vue";
import Questions from "../views/Questions.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/module/:moduleName?",
      name: "Module",
      component: Module,
    },
    {
      path: "/questions/:moduleName",
      name: "Questions",
      component: Questions,
    },
  ],
});

export default router;
