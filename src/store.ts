import { defineStore } from "pinia";
import {
  addDoc,
  doc,
  getDoc,
  getDocs,
  deleteDoc,
  setDoc,
  Timestamp,
} from "firebase/firestore";
import {
  adminRef,
  moduleRef,
  modulesRef,
  questionsRef,
  answersRef,
} from "./firebase";
import type { User } from "firebase/auth";
// import { User } from "firebase/auth";

export type Module = {
  id: string;
  title: string;
  name: string;
  visibleFrom: Timestamp;
};
export type Question = {
  id: string;
  question: string;
  questionType: number;
};
export type AddModule = Omit<Module, "id">;

export const useStore = defineStore("main", {
  state: () => {
    return {
      user: null as User | null,
      error: "",
      isAdmin: false,
      modules: [] as Module[],
      questions: {} as { [key: string]: Question[] },
      answers: {} as { [key: string]: boolean },
    };
  },
  actions: {
    async init(user: User) {
      this.user = user;
      const moduleDocs = (await getDocs(modulesRef)).docs;
      this.modules = moduleDocs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      })) as Module[];
      getDoc(adminRef(user?.email)).then((data) => {
        this.isAdmin = data.exists();
      });
      this.modules.map(async (module) => {
        const resp = await getDoc(
          doc(answersRef, module.name + this.user.email)
        );
        this.answers[module.id] = resp.exists();
      });
    },
    async setModule(module: AddModule) {
      try {
        return await addDoc(modulesRef, module);
      } catch (e) {
        this.showError("Sparningen misslyckades");
        throw e;
      }
    },
    async getQuestions(moduleId: string, refetch = false) {
      if (refetch || !this.questions[moduleId]) {
        const questionDocs = (await getDocs(questionsRef(moduleId))).docs;
        this.questions[moduleId] = questionDocs.map((doc) => ({
          ...doc.data(),
          id: doc.id,
        })) as Question[];
      }
      return this.questions[moduleId];
    },
    async addQuestion(moduleId: string, question: Question) {
      await addDoc(questionsRef(moduleId), question);
      this.getQuestions(moduleId, true);
    },
    async removeQuestion(moduleId: string, questionId: string) {
      const docet = doc(questionsRef(moduleId), questionId);
      await deleteDoc(docet);
      this.getQuestions(moduleId, true);
    },
    async addAnswers(moduleName: string, answers: any) {
      console.log('answers',answers); //eslint-disable-line
      if (this.user?.email) {
        setDoc(doc(answersRef, moduleName + this.user.email), {
          ...answers,
          moduleName,
          email: this.user.email,
        });
      }
    },

    showError(message: string) {
      this.error = message;
      setTimeout(() => (this.error = ""), 6000);
    },
  },
  getters: {
    getModuleByName: (state) => {
      return (moduleName: string) =>
        state.modules.find((item) => item.name === moduleName);
    },
  },
});
